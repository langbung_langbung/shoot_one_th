<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/login2', "OAuth\OAuthController@login")->name("loginWithDevOne");
Route::post('/', "OAuth\OAuthController@getTokenByPasswordGrant")->name("getToken");
// Route::get('/lip', "OAuth\OAuthController@loginWithPassGrant")->name("loginWithDevOneth");
// Route::get("/menu_token","MenuController");
Auth::routes();
Route::get('/info',"Info\InfoController@getInfoAccount")->name("information");
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/redirect',"OAuth\OAuthController@requestAuth" )->name("request.autherize");

Route::get('/auther/callback',"OAuth\OAuthController@callbackAuth" );

// Route::get('/redirect2', function () {
//     $http = new GuzzleHttp\Client;

//     $response = $http->post('http://dev.passport/oauth/token', [
//         'form_params' => [
//             'grant_type' => 'password',
//             'client_id' => '6',
//             'client_secret' => 'eZlHFvbWHK3L7AkwW7xNRxRPtJcgfiHrCp0KuzDp',
//             'username' => 'pannapon6',
//             'password' => 'Ohm123456',
//             'scope' => '',
//         ],
//     ]);

    
// //    $response = $http->request('GET', '/api/user', [
// //        'headers' => [
// //            'Accept' => 'application/json',
// //            'Authorization' => 'Bearer '.$accessToken,
// //        ],
// //    ]);

//     return json_decode((string) $response->getBody(), true);
// });

// Route::get('/orders', function () {
//     // Access token has both "check-status" and "place-orders" scopes...
//     \Log::info('/orders');
//     return response()->json('Success');
// })->middleware('auth:api','scopes:check-status,place-orders');