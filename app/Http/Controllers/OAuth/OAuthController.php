<?php

namespace App\Http\Controllers\OAuth;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\DB;
use Auth;
// use DB;

class OAuthController extends Controller
{
    public function login(Request $request){
        $username = $request->input("username");
        $password = $request->input('password');
        // dd($username." ".$password);
        $result = $this->getTokenByPasswordGrant($username,$password);
//        dd($result);
        // return $result;
        // return view("home");
        if($result){
            $userModel = User::where('username', "$username")->first();
            // $userModel = $users = DB::table('users')->where('username', '=', "$username")->get();
            // dd($userModel);
            Auth::login($userModel);

            return view("home");
        }else{
            return view("auth.login");
        }
    }

    public function requestAuth () {
        $query = http_build_query([
            'client_id'     => env("CLIENT_ID"),
            'redirect_uri'  => env("REDIRECT_URI_AUTHORIZATION_CODE"), //redirect ของเว็บนี้ ที่ให้ปลื้มแก้ครับ
            'response_type' => 'code',
            'scope'         => '',
        ]);
        // return redirect(env("BACKEND_IP").'/api/oauth/getcode?'. $query);
        // dd(env("BACKEND_IP").'/api/oauth/getcode?'. $query);
        return redirect(env("BACKEND_IP").'/api/oauth/getcode?'. $query);
    }

    public function callbackAuth(Request $request) {
//         dd('call back');
        $http = new Client;
        if ($request->code) {
            //dd($request->code);
            // $response = $http->post('http://dev.rita.backend/oauth/token', [
           $response = $http->post(env("BACKEND_IP")."/oauth/token", [
                'form_params' => [
                    'grant_type'    => 'authorization_code',
                    'client_id'     => env("CLIENT_ID"),
                    'client_secret' => env("CLIENT_SECRET"),
                    'redirect_uri'  => env("REDIRECT_URI_AUTHORIZATION_CODE"),
                    'code'          => $request->code,
                    'scope' => '',
                ],
            ]);
            $json = json_decode($response->getBody(), true);
            $accessToken = $json['access_token'];
            // dd($accessToken);
            if($accessToken){
//                $userModel = User::where('username', "$username")->first();
//                // $userModel = $users = DB::table('users')->where('username', '=', "$username")->get();
//                // dd($userModel);
//                Auth::login($userModel);
//                return view("home");
                $accountInfo = $this->getAccount($accessToken);
                dd($accountInfo);
            }
            return $accountInfo;
        } else {
            return response()->json(['error' => request('error')]);
        }
    }



    public function getTokenByPasswordGrant($username,$password){
        // return "test";
        // $username = $request->input("username");
        // $password = $request->input('password');
        // dd($username." ".$password );

        // dd(env('BACKEND_IP'));
        $client = new Client(['base_uri' => env('BACKEND_IP'), 'verify' => false]);
        // $client = new Client();
        // dd($client);
        $response = $client->post('/api/oauth/getpwd', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => "6",
                'client_secret' => "eZlHFvbWHK3L7AkwW7xNRxRPtJcgfiHrCp0KuzDp",
                'username' => $username,
                'password' => $password,
                // 'scope' => '',
            ],
            'http_errors' => false,
        ]);

//         dd($client);
//         dd($response);
//         dd(json_decode((string)$response->getbody(),true));
        if ($response->getStatusCode() == 200) {
            $accessToken = json_decode((string) $response->getBody(), true)['access_token'];
            session(["accessToken"=>$accessToken]);
            // dd(session("accessToken"));
            $this->getUserAndCreate($accessToken, $username, $password);
            return true;
        }
        // return $response->getStatusCode() == 200 ? true : false;
        return false;
    }

    // private function logingIn($username)
    // {
    //     $userModel = User::where('username', $username)->first();
    //     Auth::login($userModel);
    //     $this->sendLoginResponse(\request())->throwResponse();
    // }

    public function getAccount($accessToken){
        $client = new Client(['base_uri' => env('BACKEND_IP'), "verify" => false]);
        $response = $client->get('/api/account', [
            'headers' => ['Authorization' => 'Bearer ' . $accessToken],
            'http_errors' => false,
        ]);
        // dd($jsonResponse = json_decode($response->getBody(), true));
        $account = json_decode($response->getBody(), true);

        return $account;
    }

    public function getAccessToken(){
        return session("accessToken");
    }

    private function getUserAndCreate($accessToken, $username, $password)
    {
//        dd("in get user and create");
        // dd($jsonResponse = json_decode($response->getBody(), true));
        $jsonResponse = $this->getAccount($accessToken);
        // dd($jsonResponse);
        $userModel = User::where('username', "$username")->first();
//        dd($userModel);
        // User::where('username', "$username")->update(["updated_at" => Carbon::now()]);
        if (!$userModel)
        {
            $userModel = new User();
            $userModel->id = Uuid::generate()->string;
            $userModel->username = $username;
            $userModel->password = bcrypt($password);
            $userModel->id_passport = $jsonResponse['id_card_num'];
            $userModel->name = $jsonResponse['first_name_eng'];
            $userModel->surname = $jsonResponse['last_name_eng'];
            $userModel->birth_date = $jsonResponse['birth_date'];
            $userModel->phone = $jsonResponse['mobile'][0]['mobile_no'];
            $userModel->nameTH = $jsonResponse['first_name_th'];
            $userModel->surnameTH = $jsonResponse['last_name_th'];
            $userModel->has_onecard = 'I';
            // $userModel->remember_token = $accessToken;
            $userModel->save();
        }

    }

    public function loginWithPassGrant(){
        return view("login");
    }
}
