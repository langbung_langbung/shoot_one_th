<?php

namespace App\Http\Controllers\Info;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use App\Http\Controllers\OAuth\OAuthController;
class InfoController extends Controller
{
    public function getInfoAccount(){
        // dd(session("accessToken"));
        // $account = new OAutController();
        $accessToken = session("accessToken");
        $dataAcount = app('App\Http\Controllers\OAuth\OAuthController')->getAccount($accessToken);
        return $dataAcount;
    }
    //
}
